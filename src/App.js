import logo from "./logo.svg";
import "./App.css";
import Ex_ShoeShopRedux from "./Ex_ShoeShop/Ex_ShoeShopRedux";

function App() {
  return (
    <div className="App">
      <Ex_ShoeShopRedux />
    </div>
  );
}

export default App;
