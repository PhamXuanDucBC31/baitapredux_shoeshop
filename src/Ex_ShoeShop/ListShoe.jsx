import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderShoe = () => {
    return this.props.shoe_Arr.map((item) => {
      return (
        <ItemShoe handleAddToCart={() => {}} shoeData={item} key={item.id} />
      );
    });
  };

  render() {
    return <div className="row">{this.renderShoe()}</div>;
  }
}

let mapStateToProps = (state) => {
  return {
    shoe_Arr: state.shoeShopReducer.state.shoeArr,
  };
};

export default connect(mapStateToProps)(ListShoe);
