import React, { Component } from "react";
import { shoeArr } from "./data_ShoeShop";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";
import TableGioHang from "./TableGioHang";

export default class Ex_ShoeShopRedux extends Component {
  render() {
    return (
      <div className="container py-2">
        <TableGioHang />

        <ListShoe />
      </div>
    );
  }
}
