import { shoeArr } from "../../data_ShoeShop";
import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  REMOVE_ITEM,
} from "../constant/shoeShopConstant";

let initalState = {
  state: {
    shoeArr: shoeArr,
    gioHang: [],
  },
};

export let shoeShopReducer = (state = initalState, { type, payload, step }) => {
  switch (type) {
    case ADD_TO_CART: {
      let index = state.state.gioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      let cloneGioHang = [...state.state.gioHang];
      if (index == -1) {
        let newSp = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSp);
      } else {
        cloneGioHang[index].soLuong++;
      }

      state.state.gioHang = cloneGioHang;
      return { ...state };
    }

    case REMOVE_ITEM: {
      let index = state.state.gioHang.findIndex((item) => {
        return item.id == payload;
      });

      if (index !== -1) {
        let cloneGioHang = [...state.state.gioHang];
        cloneGioHang.splice(index, 1);
        state.state.gioHang = cloneGioHang;
        return { ...state };
      }
    }

    case CHANGE_QUANTITY: {
      let index = state.state.gioHang.findIndex((item) => {
        return item.id == payload;
      });

      let cloneGioHang = [...state.state.gioHang];

      cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + step;

      if (cloneGioHang[index].soLuong == 0) {
        cloneGioHang.splice(index, 1);
      }

      state.state.gioHang = cloneGioHang;
      return { ...state };
    }

    default: {
      return state;
    }
  }
};
