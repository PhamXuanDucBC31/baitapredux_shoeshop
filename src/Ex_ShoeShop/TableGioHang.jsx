import React, { Component } from "react";
import { connect } from "react-redux";
import {
  CHANGE_QUANTITY,
  REMOVE_ITEM,
} from "./redux/constant/shoeShopConstant";

class TableGioHang extends Component {
  renderContent = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.changeQuantity(item.id, +1);
              }}
              className="btn btn-success mr-2"
            >
              +
            </button>
            {item.soLuong}
            <button
              onClick={() => {
                this.props.changeQuantity(item.id, -1);
              }}
              className="btn btn-warning ml-2"
            >
              -
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handelRemoveShoe(item.id);
              }}
              className="btn btn-danger"
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>

          <tbody>{this.renderContent()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeShopReducer.state.gioHang,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handelRemoveShoe: (id) => {
      dispatch({
        type: REMOVE_ITEM,
        payload: id,
      });
    },

    changeQuantity: (id, step) => {
      dispatch({
        type: CHANGE_QUANTITY,
        payload: id,
        step: step,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TableGioHang);
